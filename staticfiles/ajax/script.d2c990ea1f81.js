// $(document).ready(() =>{
//     $('#button').click(function(){
//         var key = $('#search').val();

//         $.ajax({
//             method: 'GET',
//             url: 'http://localhost:8000/getData?key=<keyword>' + key,
//             success: function(response){
//                 console.log(response)
//                     var result ='<tr>';
//                     for(let i=0; i<response.items.length; i++){
//                         result += "<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
//                         $('.fontisi').append('<td class= align-middle >' + response.items[i].volumeInfo.title + '</td>');
//                         $('.fontisi').append('<td class= align-middle ><img src='+ response.items[i].volumeInfo.imageLinks.smallThumbnail + '></td>');
//                         $('.fontisi').append('<td class= align-middle >' + response.items[i].volumeInfo.authors + '</td>');

//                         // console.log(response.items[i].volumeInfo.authors);
//                     // }

//                 }

//             }
//         })
//     })
// })

$(document).ready(()=>{
    $('#button').click(function(){
        var key= $('#search').val();

        $.ajax({
            method:'GET',
            url :'http://localhost:8000/getData?key=' + key,
            success:function(response){

                let all = response.items

                for(let i=0;i<response.items.length;i++) {
                    let book = all[i].volumeInfo
                    var name = $('<td>').text(book.title);

                    if ('publisher' in book == false) var publisher =  $('<td>').text('-');
               
                    else var publisher = $('<td>').text(book.publisher);

                    if ('pageCount' in book == false) var pageCount = $('<td>').text("-");

                    else var pageCount = $('<td>').text(book.pageCount);

                    if ('authors' in book == false) var authors = $('<td>').text("-");

                    else var authors = $('<td>').text(book.authors);

                    if ('categories' in book == false) var categories = $('<td>').text("-");

                    else var categories = $('<td>').text(book.categories);

                    if ('imageLinks' in book == false)
                        var img = $('<td>').text("-");
                    else {
                        if ('smallThumbnail' in book.imageLinks == false)
                            var img = $('<td>').append($('<img>').attr({
                                'src': book.imageLinks.thumbnail
                            }));
                        else
                            var img = $('<td>').append($('<img>').attr({
                                'src': book.imageLinks.smallThumbnail
                            }));
                    }

                    var tr = $('<tr>').append(name, authors,
                        pageCount, publisher, categories, img);

                    $('tbody').append(tr);

                }


            }

        })
    })
})