from django.shortcuts import render, redirect
from django.http import JsonResponse
import requests

def story8ola(request):
    return render(request, 'story8.html')

def get_data(request):
    key = request.GET['key']
    url ='https://www.googleapis.com/books/v1/volumes?q=' + key

    response = requests.get(url)
    response_json = response.json()

    return JsonResponse(response_json)