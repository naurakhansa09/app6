from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import request
from importlib import import_module
from django.urls import resolve
from django.conf import settings

from django.test import TestCase
from django.test import TestCase,Client,LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import unittest
import time

from .views import *


# Create your tests here.

class UnitTest(TestCase):
    def test_ada_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_html(self):
        response = Client().get('/app8/')
        self.assertTemplateUsed(response, 'story8.html')

    # def test_ada_searchbar(self):
    #     c = Client()
    #     response = c.get('/app8/')
    #     content = response.content.decode('utf8') 
    #     self.assertIn ("<button", content) 
    #     self.assertIn("Search this book", content)


    def test_ada_tabel(self):
        response = Client().get('/app8/')
        content = response.content.decode('utf8')
        self.assertIn ("<table", content) 

class FunctionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(25)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_searchbar_working_and_showing_filtered_search(self):
        self.browser.get('http://127.0.0.1:8000/app8')

        searchbox = self.browser.find_element_by_id("search")
        searchbox.send_keys("teknik")

        time.sleep(2)
        
        searchbutton = self.browser.find_element_by_id("button")
        searchbutton.click()

        time.sleep(2)




       




        




