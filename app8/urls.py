from django.contrib import admin 
from django.urls import path, include 
from . views import story8ola, get_data

urlpatterns =[
    path('app8/', story8ola),
    path('getData/', get_data)
]
