from django import forms
from .models import StatusForm

class Status(forms.ModelForm) :

    class Meta :
        model = StatusForm
        fields =['status']
        

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control form-horizontal control-label'
             })
