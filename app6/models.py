from django.db import models

class StatusForm(models.Model):
    status = models.CharField(max_length=150)
    date = models.DateTimeField(auto_now=True)

