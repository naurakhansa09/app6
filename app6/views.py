from django.shortcuts import render,redirect

from .forms import Status
from .models import StatusForm

def StatusPost(request):
    if request.method == 'POST' :
        form = Status(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('StatusPost')
    else :
        form=Status()
    AllJadwal = StatusForm.objects.all()
 
    args={
        'form':form,
        'AllJadwal':AllJadwal,
  
    }
    
    return render(request,'index.html',args)
