from django.test import TestCase, Client
from . import models, forms

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time



class TestStatusPage(TestCase) :
    def test_url_ada(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_html_status(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_form_konten_status(self):
        form_konten = {
            'status' : 'halo'   
        }
        status = forms.Status(data = form_konten) 
        self.assertTrue(status.is_valid())
    
    def test_model_konten_status(self):
        var1 = models.StatusForm.objects.all().count()  
        statusmodel = models.StatusForm.objects.create(status="halo") 
        var2 = models.StatusForm.objects.all().count() 
        self.assertEqual(var1, 0)
        self.assertEqual(var2, var1+1)
        self.assertTrue(isinstance(statusmodel, models.StatusForm)) 
    
    def test_button_add(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8') 
        self.assertIn ("<button", content) 
        self.assertIn("Add Status", content)

class FunctionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(25)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_post(self):
        self.browser.get('http://127.0.0.1:8000/')
        self.assertIn('Status', self.browser.title)

        text = self.browser.find_element_by_tag_name('p').text
        self.assertIn('Halo, apa kabar?', text)

        inputbox1 = self.browser.find_element_by_id('id_status')
        time.sleep(1)
        inputbox1.send_keys('Halo')
        time.sleep(2)
        inputbox1.send_keys(Keys.ENTER)
        time.sleep(5)

        self.assertIn('Halo', self.browser.page_source)

