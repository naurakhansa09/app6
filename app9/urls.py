from django.contrib import admin 
from django.urls import path, include 
from . views import user_login, success, user_logout

urlpatterns =[
    path('login/', user_login, name= "user_login"),
    path('success/', success, name = "user_success"), 
    path('logout/', user_logout, name ="user_logout"),

]