from django.test import TestCase
from django.test import TestCase,Client,LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import unittest
import time

class story9(TestCase) :
    def test_ada_url_halaman_login(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    # def test_ada_url_halaman_success(self):
    #     response = Client().get('/success/')
    #     self.assertEqual(response.status_code, 200)
    
    def test_html_login(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_html_success(self):
        response = Client().get('/success/')
        self.assertTemplateUsed(response, 'success.html')

    # def test_kalaugaada_url_login_return_404(self):
    #     c = Client()
    #     response = c.get('/login/')
    #     self.assertEqual(response.status_code, 404)

    # def test_kalaugaada_url_success_return_404(self):
    #     c = Client()
    #     response = c.get('/success/')
    #     self.assertEqual(response.status_code, 404)

    def test_button_login(self):
        c = Client()
        response = c.get('/login/')
        content = response.content.decode('utf8') 
        self.assertIn ("<button", content) 
        self.assertIn("Login", content)

    def test_button_logout(self):
        c = Client()
        response = c.get('/success/')
        content = response.content.decode('utf8') 
        self.assertIn ("<button", content) 
        self.assertIn("Logout", content)

#  def test_konten_ada_kalimat_welcome(self):
#         c = Client()
#         response = c.get('/success/')
#         content = response.content.decode('utf8')
#         self.assertIn ("", body)

class FunctionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(25)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    
    def test_login_and_logout_working_and_showing_message(self):
        self.browser.get('http://127.0.0.1:8000/login/')

        inputusername = self.browser.find_element_by_id("username")
        inputpass = self.browser.find_element_by_id("password")
        loginbtn = self.browser.find_element_by_id("login")

        inputusername.send_keys("olaa")
        inputpass.send_keys("halo1234")
        loginbtn.click()
        time.sleep(2)

        self.assertEqual(self.browser.current_url, "http://127.0.0.1:8000/success/")
        # time.sleep(2)

        logoutbtn = self.browser.find_element_by_id("logout")
        logoutbtn.click()
        time.sleep(2)
        self.assertEqual(self.browser.current_url, "http://127.0.0.1:8000/login/")
        time.sleep(5)




