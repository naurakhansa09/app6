from django.test import TestCase
from django.test import TestCase,Client,LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import unittest
import time

class story7olaPage(TestCase) :
    def test_ada_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
    
    def test_html(self):
        response = Client().get('/app7/')
        self.assertTemplateUsed(response, 'story7ola.html')
    
    # def test_ada_title(self):
    #     c = Client()
    #     response = c.get('/app7/')
    #     content = response.content.decode('utf8')
    #     self.assertIn ("<title>Olas website</title>", content)

    # def test_kalaugaada_url_return_404(self):
    #     c = Client()
    #     response = c.get('/app7/')
    #     self.assertEqual(response.status_code, 404)

    def test_konten_ada_nama(self):
        c = Client()
        response = c.get('/app7/')
        content = response.content.decode('utf8')
        self.assertIn ("Naura Khansa Priono", content)

    def test_konten_ada_daftar_aktivitas(self):
        c = Client()
        response = c.get('/app7/')
        content = response.content.decode('utf8')
        self.assertIn ("Daftar Aktivitas", content)
    
    def test_konten_ada_daftar_pengalaman(self):
        c = Client()
        response = c.get('/app7/')
        content = response.content.decode('utf8')
        self.assertIn ("Pengalaman Organisasi/Kepanitiaan", content)

    def test_konten_ada_daftar_prestasi(self):
        c = Client()
        response = c.get('/app7/')
        content = response.content.decode('utf8')
        self.assertIn ("Prestasi", content)

    def test_ada_toggle(self):
        c = Client()
        response = c.get('/app7/')
        content = response.content.decode('utf8')
        self.assertIn ("toggle", content)

    def test_ada_accordion(self):
        c = Client()
        response = c.get('/app7/')
        content = response.content.decode('utf8')
        self.assertIn ("accordion", content)

class FunctionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(25)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_dark_mode_light_mode(self):
        # selenium = self.selenium
        self.browser.get('http://127.0.0.1:8000/app7')

        time.sleep(5)
        e = self.browser.find_element_by_css_selector(".toggle")
        time.sleep(2)
        e.click()
        time.sleep(2)

        light = "rgba(247, 247, 247, 1)"
        dark = "rgba(1, 1, 1, 1)"

        cbl = "rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box"
        cbn = "rgb(42, 42, 42) none repeat scroll 0% 0% / auto padding-box border-box"

        chl= "rgb(155, 198, 201) none repeat scroll 0% 0% / auto padding-box border-box"
        chn= "rgb(172, 219, 223) none repeat scroll 0% 0% / auto padding-box border-box"

        lh1= "rgba(33, 37, 41, 1)"
        nh1= "rgba(255, 255, 255, 1)"


        bg = self.browser.find_element_by_tag_name("body").value_of_css_property("background-color")
        self.assertEqual(bg, dark)

        h1 = self.browser.find_element_by_tag_name("h1").value_of_css_property("color")
        self.assertEqual(nh1, h1)

        cb = self.browser.find_element_by_class_name("card-body").value_of_css_property("background")
        self.assertEqual(cb, cbn)

        ch = self.browser.find_element_by_class_name("card-header").value_of_css_property("background")
        self.assertEqual(chn, ch)

        e.click()
        time.sleep(2)

        bg = self.browser.find_element_by_tag_name("body").value_of_css_property("background-color")
        self.assertEqual(bg, light)

        h1 = self.browser.find_element_by_tag_name("h1").value_of_css_property("color")
        self.assertEqual(lh1, h1)

        cb = self.browser.find_element_by_class_name("card-body").value_of_css_property("background")
        self.assertEqual(cbl, cb)

        ch = self.browser.find_element_by_class_name("card-header").value_of_css_property("background")
        self.assertEqual(chl, ch)

        time.sleep(2)

    def test_klik_accordion(self):
        self.browser.get('http://127.0.0.1:8000/app7')
        
        # find the element
        acc1 = self.browser.find_element_by_id('headingOne')
        acc2 = self.browser.find_element_by_id('headingTwo')
        acc3 = self.browser.find_element_by_id('headingThree')

        # test
        time.sleep(1)
        acc2.click()
        time.sleep(1)
        acc3.click()
        time.sleep(1)
        acc1.click()
        time.sleep(1)



   
        
    
    
